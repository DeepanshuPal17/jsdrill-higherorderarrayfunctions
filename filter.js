module.exports = function filter(elements,cb)
{
    if(Array.isArray(elements))
    {
        let arr=[];
        if(elements.length !==0 && typeof cb === 'function')
        {
            
            let index=0;
            for(let i=0;i<elements.length;i++)
            {
                if(cb(elements[i],i,elements))
                {
                    arr[index]=elements[i];
                    index++;
                }
            }
            
        }
        return arr;
    }
}