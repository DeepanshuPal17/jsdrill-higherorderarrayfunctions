module.exports=function map(elements,cb){
    let arr=[];
    if(Array.isArray(elements))
    {
        if(elements.length!==0&& typeof cb === 'function')
        {
            for(let index=0;index<elements.length;index++)
            {
                arr[index] = cb(elements[index],index,elements);
            }
        }
    }
    return arr;

}