module.exports=function reduce(elements, cb , startingValue)
{
    let value;
    if(Array.isArray(elements))
    {
        if(elements.length!==0 && typeof cb === 'function')
        {
            let i=0;
            if(startingValue === undefined)
            {
                startingValue=elements[0];
                i++;
            }
            value =startingValue
            for(;i<elements.length;i++)
            {
                value=cb(value,elements[i],i,elements)
            }
            return value;
        }
    }
    
}