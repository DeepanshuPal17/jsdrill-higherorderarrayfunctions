module.exports = function find(elements , cb)
{
    if(Array.isArray(elements))
    {
        if(elements.length !==0 && typeof cb === 'function')
        {
            for(let index=0; index<elements.length;index++)
            {
                if(cb(elements[index],index,elements))
                {
                    return elements[index];
                }
            }
        }
    }
    return undefined;
}