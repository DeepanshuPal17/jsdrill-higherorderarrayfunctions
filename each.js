module.exports=function each(elements,cb){
    if(Array.isArray(elements))
    {
        if(elements.length!==0&& typeof cb === 'function')
        {
            for(let index=0;index<elements.length;index++)
            {
                cb(elements[index],index,elements);
            }
        }
    }
}