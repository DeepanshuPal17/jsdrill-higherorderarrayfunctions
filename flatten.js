module.exports = function filter(elements,value=1)
{
    let arr=[];
    if(Array.isArray(elements))
    {
        if(value<1)
        return elements;
        for(let i=0;i<elements.length; i++)
        {
            if(Array.isArray(elements[i]))
            {
                arr=arr.concat(filter(elements[i],value-1));
            }
            else{
                arr.push(elements[i]);
            }
        }
    }
    return arr;
}